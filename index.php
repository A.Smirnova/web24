<?php

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages['OK'] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  $errors['superpowers'] = !empty($_COOKIE['superpowers_error']);


  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['email']){

    setcookie('email_error', '', 100000);
    $messages['email'] = '<div class="error">Заполните email.</div>';
}

if ($errors['gender']){
    $messages['gender'] = '<div class="error">Выберите пол.</div>';
    setcookie('gender_error', '', 100000);
}
if ($errors['limbs']){
    $messages['limbs'] = '<div class="error">Выберите количество конечностей.</div>';
    setcookie('limbs_error', '', 100000);
}
if ($errors['year']){
    $messages['year'] = '<div class="error">Выберите год рождения.</div>';
    setcookie('year_error', '', 100000);
}
if ($errors['biography']){
    $messages['biography'] = '<div class="error">Заполните биографию.</div>';
    setcookie('biography_error', '', 100000);
}
if ($errors['check']){
    $messages['check'] = '<div class="error">Ознакомьтесь с контрактом.</div>';
    setcookie('check_error', '', 100000);
}
if ($errors['superpowers']){
        $messages['superpowers'] = '<div class="error">Выберите суперспособности.</div>';
        setcookie('superpowers_error', '', 100000);
}

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
  $values['superpowers'] = empty($_COOKIE['superpowers']) ? '' : unserialize($_COOKIE['superpowers']);
  
  include('forma.php');
}
  
  else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['name'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле email.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['gender'])) {
        // Выдаем куку на день с флажком об ошибке в поле gender.
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['superpowers'])) {
        // Выдаем куку на день с флажком об ошибке в поле superpower.
        setcookie('superpowers_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('superpowers', serialize($_POST['superpowers']), time() + 30 * 24 * 60 * 60);
        }
    if (empty($_POST['limbs'])) {
        // Выдаем куку на день с флажком об ошибке в поле limbs.
        setcookie('limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['year'])) {
        // Выдаем куку на день с флажком об ошибке в поле year.
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['biography'])) {
        // Выдаем куку на день с флажком об ошибке в поле biography.
        setcookie('biography_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['check'])) {
        // Выдаем куку на день с флажком об ошибке в поле check.
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('name_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('check_error', '', 100000);
    }

$user = 'u20622';
$pass = '2079517';
    try {
        $pdo = new PDO('mysql:host=localhost;dbname=u20622', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		
		$name = $_POST['name'];
		$email = $_POST['email'];
		$year = $_POST['year'];
		$gender = $_POST['gender'];
		$limbs = $_POST['limbs'];
		$biography = $_POST['biography'];
		
		$sql = "insert into info (name, email, birth, gender, limbs, biography) values (:name, :email, :year, :gender, :limbs, :biography)";
		$query = $pdo->prepare($sql);
		$query->execute(['name' => $name, 'email' => $email, 'year' => $year, 'gender' => $gender, 'limbs' => $limbs, 'biography' => $biography]);
		
		$id = $pdo->lastInsertId();
		
		$quer = $pdo->prepare('INSERT INTO connection (id, id_power) VALUES (:id, :id_power)');
		
		foreach ($_POST['superpower'] as $superpower){
			$quer->execute(['id' => $id, 'id_power' => $superpower]);
		}
    }
    catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    header('Location: index.php');
}

?>
